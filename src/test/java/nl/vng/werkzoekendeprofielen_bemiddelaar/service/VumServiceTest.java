package nl.vng.werkzoekendeprofielen_bemiddelaar.service;

import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeMatchingProfielen;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesCallbackRequest;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.AanvraagWerkzoekende;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.MPWerkzoekendeMatch;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.VraagIdNotFoundException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.repository.AanvraagWerkzoekendeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VumServiceTest {

    public static final String VRAAG_ID = "VRAAG_ID";
    @Mock
    private AanvraagWerkzoekendeRepository repository;

    @InjectMocks
    private VumService vumService;

    @Test
    void shouldHandleCallback() {

        WerkzoekendeProfielMatchesCallbackRequest callbackRequest = new WerkzoekendeProfielMatchesCallbackRequest();
        callbackRequest.setVraagID(VRAAG_ID);
        callbackRequest.setMatches(new WerkzoekendeMatchingProfielen());
        callbackRequest.getMatches().setMpWerkzoekendeMatches(Arrays.asList(new MPWerkzoekendeMatch()));
        Optional<AanvraagWerkzoekende> aanvraagInDbOpt = Optional.of(new AanvraagWerkzoekende());
        when(repository.findById(callbackRequest.getVraagID())).thenReturn(aanvraagInDbOpt);

        vumService.handleCallback(callbackRequest);

        verify(repository).findById(callbackRequest.getVraagID());
        verify(repository).save(any());

    }

    @Test
    void shouldHandleNotKnownVraag() {

        WerkzoekendeProfielMatchesCallbackRequest callbackRequest = new WerkzoekendeProfielMatchesCallbackRequest();
        callbackRequest.setVraagID(VRAAG_ID);
        callbackRequest.setMatches(new WerkzoekendeMatchingProfielen());
        callbackRequest.getMatches().setMpWerkzoekendeMatches(Arrays.asList(new MPWerkzoekendeMatch()));
        Optional<AanvraagWerkzoekende> aanvraagInDbOpt = Optional.empty();
        when(repository.findById(callbackRequest.getVraagID())).thenReturn(aanvraagInDbOpt);

        Assertions.assertThrows(VraagIdNotFoundException.class, () -> vumService.handleCallback(callbackRequest), "400 BAD_REQUEST \"Vraag ID niet gevonden\"");
    }
}