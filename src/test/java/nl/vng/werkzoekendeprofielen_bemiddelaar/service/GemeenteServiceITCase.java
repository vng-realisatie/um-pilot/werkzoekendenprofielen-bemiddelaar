package nl.vng.werkzoekendeprofielen_bemiddelaar.service;

import nl.vng.werkzoekendeprofielen_bemiddelaar.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.InlineResponse200;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.InlineResponse2001;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesRequest;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesRequestGemeente;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.AanvraagWerkzoekende;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.NoMoreAccessRequestsLeftException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.UnprocessableException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.VraagIdNotFoundException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.mapper.SimpleMapper;
import nl.vng.werkzoekendeprofielen_bemiddelaar.repository.AanvraagWerkzoekendeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@JsonTest
@ActiveProfiles("test")
class GemeenteServiceITCase {

    @Mock
    private AanvraagWerkzoekendeRepository repository;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private SimpleMapper mapper;
    @Mock
    private ConfigProperties properties;

    @Autowired
    private ObjectMapper objectMapper;

    private GemeenteService service;

    private static final String URL = "example.com";
    private static final String VUM_URL = "http://example2.com";
    private static final String OIN = "123456789";
    private static final String VRAAG_ID = "somevraagid";
    private static final String VUM_ID = "somevumid";
    private static final String PATH = "./src/test/resources/";

    private WerkzoekendeProfielMatchesRequestGemeente requestGemeente;
    private WerkzoekendeProfielMatchesRequest request;
    private InlineResponse200 responseToMatchesRequest;
    private InlineResponse2001 responseToDetailRequest;
    private AanvraagWerkzoekende aanvraagWerkzoekende;

    @BeforeEach
    void setUp() {
        try {
            service = new GemeenteService(repository, restTemplate, mapper, properties);
            requestGemeente = objectMapper.readValue(new File(PATH, "werkzoekende-profiel-matches-request-gemeente.json"), WerkzoekendeProfielMatchesRequestGemeente.class);
            request = objectMapper.readValue(new File(PATH, "werkzoekende-profiel-matches-request.json"),
                    WerkzoekendeProfielMatchesRequest.class);
            responseToMatchesRequest = objectMapper.readValue(new File(PATH, "inline-response200.json"),
                    InlineResponse200.class);
            aanvraagWerkzoekende = objectMapper.readValue(new File(PATH, "aanvraag-werkzoekende.json"),
                    AanvraagWerkzoekende.class);
            responseToDetailRequest = objectMapper.readValue(new File(PATH, "inline-response2001.json"),
                    InlineResponse2001.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void findByVraagIdAndOinSuccess() {
        when(repository.findByVraagIdAndOin(VRAAG_ID, OIN)).thenReturn(aanvraagWerkzoekende);

        assertThat(service.findByVraagIdAndOin(VRAAG_ID, OIN)).isEqualTo(aanvraagWerkzoekende);
    }

    @Test
    void findByVraagIdAndOinFailure() {
        when(repository.findByVraagIdAndOin(VRAAG_ID, OIN)).thenReturn(null);

        assertThatThrownBy(() -> {
            service.findByVraagIdAndOin(VRAAG_ID, OIN);
        }).isInstanceOf(VraagIdNotFoundException.class)
                .hasMessage("400 BAD_REQUEST \"Vraag ID niet gevonden\"");
    }

    @Test
    void makeRequestMatchesVumCorrect() {
        when(properties.getCallbackUrl()).thenReturn(URL);
        when(properties.getVumUrlMatches()).thenReturn(VUM_URL);
        when(mapper.requestGemeenteToWerkzoekendeProfielMatchesRequest(requestGemeente))
                .thenReturn(request);
        when(restTemplate.postForObject(eq(VUM_URL), any(), eq(InlineResponse200.class)))
                .thenReturn(responseToMatchesRequest);

        InlineResponse200 result = service.makeRequestMatchesVum(requestGemeente, OIN);

        assertThat(result).isEqualTo(responseToMatchesRequest);
    }

    @Test
    void makeRequestMatchesVumVraagIdAlreadyInDB() {
        responseToMatchesRequest.setVraagID(VRAAG_ID);

        when(properties.getCallbackUrl()).thenReturn(URL);
        when(properties.getVumUrlMatches()).thenReturn(VUM_URL);
        when(mapper.requestGemeenteToWerkzoekendeProfielMatchesRequest(requestGemeente))
                .thenReturn(request);
        when(restTemplate.postForObject(eq(VUM_URL), any(), eq(InlineResponse200.class)))
                .thenReturn(responseToMatchesRequest);
        when(repository.existsById(VRAAG_ID)).thenReturn(true);

        assertThatThrownBy(() -> {
            service.makeRequestMatchesVum(requestGemeente, OIN);
        }).isInstanceOf(UnprocessableException.class)
                .hasMessage("422 UNPROCESSABLE_ENTITY \"Vraag id bestaat al in database\"");
    }

    @Test
    void makeRequestMatchesVumNullResponse() {
        when(properties.getCallbackUrl()).thenReturn(URL);
        when(properties.getVumUrlMatches()).thenReturn(VUM_URL);
        when(mapper.requestGemeenteToWerkzoekendeProfielMatchesRequest(requestGemeente))
                .thenReturn(request);

        assertThatThrownBy(() -> {
            service.makeRequestMatchesVum(requestGemeente, OIN);
        }).isInstanceOf(UnprocessableException.class)
                .hasMessage("422 UNPROCESSABLE_ENTITY \"Rest client error\"");

    }

    @Test
    void makeRequestMatchesVumRestClientException() {
        when(properties.getCallbackUrl()).thenReturn(URL);
        when(properties.getVumUrlMatches()).thenReturn(VUM_URL);
        when(mapper.requestGemeenteToWerkzoekendeProfielMatchesRequest(requestGemeente))
                .thenReturn(request);
        
        assertThatThrownBy(() -> {
            service.makeRequestMatchesVum(requestGemeente, OIN);
        }).isInstanceOf(UnprocessableException.class)
                .hasMessage("422 UNPROCESSABLE_ENTITY \"Rest client error\"");

    }

    @Test
    void findAllReturnEmpty() {
        when(repository.findByOin(OIN)).thenReturn(List.of());

        assertThat(service.findAll(OIN)).isEmpty();
    }

    @Test
    void findAllReturnResult() {
        when(repository.findByOin(OIN)).thenReturn(List.of(aanvraagWerkzoekende));

        assertThat(service.findAll(OIN)).hasSameElementsAs(List.of(aanvraagWerkzoekende));
    }

    @Test
    void makeRequestDetailWerkzoekendeVraagIdNotFoundFailure() {
        when(repository.findByVraagIdAndOin(VRAAG_ID, OIN)).thenReturn(null);

        assertThatThrownBy(() -> {
            service.makeRequestDetailWerkzoekende(VRAAG_ID, VUM_ID, OIN);
        }).isInstanceOf(VraagIdNotFoundException.class)
                .hasMessage("400 BAD_REQUEST \"Vraag ID niet gevonden\"");
    }

    @Test
    void makeRequestDetailWerkzoekendeNoAccessLeftFailure() {
        aanvraagWerkzoekende.setTimesAccessLeft(0);

        when(repository.findByVraagIdAndOin(VRAAG_ID, OIN)).thenReturn(aanvraagWerkzoekende);

        assertThatThrownBy(() -> {
            service.makeRequestDetailWerkzoekende(VRAAG_ID, VUM_ID, OIN);
        }).isInstanceOf(NoMoreAccessRequestsLeftException.class)
                .hasMessage("429 TOO_MANY_REQUESTS \"limiet is overschreden voor deze vraagID\"");
    }

    @Test
    void makeRequestDetailWerkzoekendeRestClientFailure() {
        // Still have access left.
        aanvraagWerkzoekende.setTimesAccessLeft(10);

        when(properties.getVumUrlVumId()).thenReturn(VUM_URL);
        when(repository.findByVraagIdAndOin(VRAAG_ID, OIN)).thenReturn(aanvraagWerkzoekende);

        assertThatThrownBy(() -> {
            service.makeRequestDetailWerkzoekende(VRAAG_ID, VUM_ID, OIN);
        }).isInstanceOf(UnprocessableException.class)
                .hasMessage("422 UNPROCESSABLE_ENTITY \"Rest client error\"");

        assertThat(aanvraagWerkzoekende.getTimesAccessLeft()).isEqualTo(10);

    }

}