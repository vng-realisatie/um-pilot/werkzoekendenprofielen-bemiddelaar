package nl.vng.werkzoekendeprofielen_bemiddelaar;

import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.Werkzoekende;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * User: philip
 * Date: 1/2/14
 * Time: 8:11 AM
 */
public class GetterAndSetterTest {

    @Test
    public void shouldTestAllGetterAndSetterPairs() throws Exception {

        // set parameter to true to print extended report
        GetterAndSetterVisitor visitor = new GetterAndSetterVisitor(true);
        FileWalker pf = new FileWalker(Werkzoekende.class, visitor);
        pf.walkTree();

        Assertions.assertEquals(visitor.getNumberFailed(), 0);
    }
}

class GetterAndSetterVisitor {

    private static final Logger log = LoggerFactory.getLogger(GetterAndSetterVisitor.class);
    public static final String NEW_LINE = "\n\r";
    public static final String SPACE = " ";
    public static final String CLASS = ".class";
    private int lengthStartingDir;
    private int numberOfTests;
    private int numberSucceeded;
    private int numberFailed;
    private final boolean printReport;
    private HashMap<String, Collection<String>> failedTests = new HashMap();

    public GetterAndSetterVisitor(boolean withDetailedReport) {
        printReport = withDetailedReport;
    }

    public void startingDir(Path path) {
        this.lengthStartingDir = path.toString().length();
    }

    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
        FileVisitResult result = FileVisitResult.CONTINUE;
        String path = file.toString();
        path = path.substring(lengthStartingDir + 1, path.length() - ".class".length());
        path = path.replace(File.separator, ".");
        path = path.replace("\\", ".");
        if (attr.isRegularFile() && file.toString().endsWith(".class")) {
            try {
                Class<?> clazz = Class.forName(path);
                result = visit(clazz);
            } catch (ClassNotFoundException var6) {
                result = FileVisitResult.CONTINUE;
            }
        }

        return result;
    }

    public void printReport() {
        StringBuilder sb = new StringBuilder("\n\r");
        this.printReportLine(sb, "Total     :", this.numberOfTests);
        this.printReportLine(sb, "Succeeded :", this.numberSucceeded);
        this.printReportLine(sb, "Failed    :", this.numberFailed);
        Iterator var2 = this.failedTests.entrySet().iterator();

        Map.Entry map;
        while(var2.hasNext()) {
            map = (Map.Entry)var2.next();
            this.printReportLine(sb, (String)map.getKey(), ((Collection)map.getValue()).size());
        }

        if (this.printReport) {
            this.printVisitDetails(sb);
            var2 = this.failedTests.entrySet().iterator();

            while(var2.hasNext()) {
                map = (Map.Entry)var2.next();
                this.printReportLine(sb, (String)map.getKey(), ((Collection)map.getValue()).size());
                Iterator var4 = ((Collection)map.getValue()).iterator();

                while(var4.hasNext()) {
                    String error = (String)var4.next();
                    sb.append(error);
                    sb.append("\n\r");
                }
            }
        }

        log.info(sb.toString());
    }

    protected void addSucceeded() {
        ++this.numberSucceeded;
    }

    protected void addFailed() {
        ++this.numberFailed;
    }

    protected void addChecked() {
        ++this.numberOfTests;
    }

    protected void addFailedTest(String exceptionName, String className) {
        synchronized(this.failedTests) {
            if (!this.failedTests.containsKey(exceptionName)) {
                this.failedTests.put(exceptionName, new ArrayList());
            }

            ((Collection)this.failedTests.get(exceptionName)).add(className);
        }
    }

    private void printReportLine(StringBuilder sb, String header, int result) {
        sb.append(header);
        sb.append(" ");
        sb.append(result);
        sb.append("\n\r");
    }

    public int getNumberOfTests() {
        return this.numberOfTests;
    }

    public int getNumberSucceeded() {
        return this.numberSucceeded;
    }

    public int getNumberFailed() {
        return this.numberFailed;
    }

    protected void printVisitDetails(StringBuilder sb) {
        sb.append("Unbalanced Getters and Setters");
        sb.append("\n\r");
        sb.append("Classes not tested with cause : ");
        sb.append("\n\r");
    }

    public FileVisitResult visit(final Class<?> clazz) {
        boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
        if (isAbstract) {
            return FileVisitResult.CONTINUE;
        } else {
            try {
                boolean hasZeroArgumentConstructor = false;
                Iterator var4 = Arrays.asList(clazz.getConstructors()).iterator();

                while(var4.hasNext()) {
                    Constructor<?> constructor = (Constructor)var4.next();
                    if (constructor.getTypeParameters().length == 0) {
                        hasZeroArgumentConstructor = true;
                        break;
                    }
                }

                if (hasZeroArgumentConstructor) {
                    runTest(clazz);
                }
            } catch (Throwable var6) {
                return FileVisitResult.CONTINUE;
            }

            return FileVisitResult.CONTINUE;
        }
    }

    private void runTest(Class<?> clazz) {
        try {
            Object instance = clazz.newInstance();
            this.testFields(instance);
        } catch (InstantiationException var3) {
            this.addFailedTest("InstantiationException", clazz.getName() + " ");
        } catch (IllegalAccessException var4) {
            this.addFailedTest("IllegalAccessException", clazz.getName() + " ");
        }

    }

    public void testFields(Object instance) {
        Class<?> clazz = instance.getClass();
        Field[] var3 = clazz.getDeclaredFields();
        int var4 = var3.length;

        for(int var5 = 0; var5 < var4; ++var5) {
            Field field = var3[var5];

            try {
                Method getter = this.findGetterMethod(clazz, field);
                Method setter = clazz.getMethod("set" + StringUtils.capitalize(field.getName()), field.getType());
                this.performTest(instance, field, getter, setter);
            } catch (InvocationTargetException var9) {
                this.addFailedTest("InvocationTargetException", clazz.getName() + " " + field.getName());
            } catch (InstantiationException var10) {
                this.addFailedTest("InstantiationException", clazz.getName() + " " + field.getName() + "  " + field.getType());
            } catch (IllegalAccessException var11) {
                this.addFailedTest("IllegalAccessException", clazz.getName() + " " + field.getName());
            } catch (NoSuchMethodException var12) {
            }
        }

    }

    private Method findGetterMethod(Class<?> clazz, Field field) throws NoSuchMethodException {
        try {
            return clazz.getMethod("get" + StringUtils.capitalize(field.getName()));
        } catch (NoSuchMethodException var4) {
            return clazz.getMethod("is" + StringUtils.capitalize(field.getName()));
        }
    }

    private void performTest(Object instance, Field field, Method getter, Method setter) throws InvocationTargetException, IllegalAccessException, InstantiationException {
        this.addChecked();
        if (field.getType().isEnum()) {
            this.testEnum(instance, field, getter, setter);
        } else if (field.getType().isPrimitive()) {
            this.testPrimitive(instance, field, getter, setter);
        } else {
            this.testObject(instance, field, getter, setter);
        }

    }

    private void testEnum(Object instance, Field field, Method getter, Method setter) throws InvocationTargetException, IllegalAccessException {
        Class<?> c = field.getType();
        this.invokeSetterAndGetter(instance, field, getter, setter, c.getEnumConstants()[0]);
    }

    private void testPrimitive(Object instance, Field field, Method getter, Method setter) throws InstantiationException, IllegalAccessException, InvocationTargetException {
        Object expected = this.createPrimitive(field);
        this.invokeSetterAndGetter(instance, field, getter, setter, expected);
    }

    private void testObject(Object instance, Field field, Method getter, Method setter) throws InstantiationException, IllegalAccessException, InvocationTargetException {
        Object expected = this.createInstance(field);
        this.invokeSetterAndGetter(instance, field, getter, setter, expected);
    }

    private void invokeSetterAndGetter(Object instance, Field field, Method getter, Method setter, Object expected) throws IllegalAccessException, InvocationTargetException {
        setter.invoke(instance, expected);
        Object result = getter.invoke(instance);
        if (ObjectUtils.equals(result, expected)) {
            this.addSucceeded();
        } else {
            this.addFailed();
            this.addFailedTest("failedGetterAndSetter", instance.getClass().getName() + " " + field.getName());
        }

    }

    private Object createInstance(Field field) throws InstantiationException, IllegalAccessException {
        if (Long.class.equals(field.getType())) {
            return -2L;
        } else if (Integer.class.equals(field.getType())) {
            return -3;
        } else if (Boolean.class.equals(field.getType())) {
            return Boolean.TRUE;
        } else if (List.class.equals(field.getType())) {
            return new ArrayList();
        } else if (BigDecimal.class.equals(field.getType())) {
            return new BigDecimal(1.1D);
        } else {
            return Map.class.equals(field.getType()) ? new HashMap() : field.getType().newInstance();
        }
    }

    private Object createPrimitive(Field field) throws IllegalAccessException, InvocationTargetException {
        Object expected = null;
        if ("int".equals(field.getType().toString())) {
            expected = -1;
        }

        if ("long".equals(field.getType().toString())) {
            expected = -1L;
        }

        if ("boolean".equals(field.getType().toString())) {
            expected = Boolean.FALSE;
        }

        return expected;
    }
}

class FileWalker extends SimpleFileVisitor<Path> {

    private Path startingDir;
    private GetterAndSetterVisitor visitor;

    public FileWalker(Class<?> clazz, GetterAndSetterVisitor visitor) throws Exception {
        this.visitor = visitor;
        URL location = clazz.getProtectionDomain().getCodeSource().getLocation();
        this.startingDir = (new File(location.toURI())).toPath();
        visitor.startingDir(this.startingDir);
    }

    public void walkTree() throws Exception {
        Files.walkFileTree(this.startingDir, this);
        visitor.printReport();
    }

    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
        return visitor.visitFile(file, attr);
    }

    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        return FileVisitResult.CONTINUE;
    }

    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        return FileVisitResult.CONTINUE;
    }
}