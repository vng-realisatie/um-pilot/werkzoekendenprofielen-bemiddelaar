package nl.vng.werkzoekendeprofielen_bemiddelaar.serializer;

import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.Telefoonnummer;
import com.fasterxml.jackson.core.JsonGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

@ExtendWith(MockitoExtension.class)
class ValueSerializerTelefoonnummerTest {

    @Test
    public void shouldSerialize() throws IOException {

        ValueSerializerTelefoonnummer serializerTelefoonnummer = new ValueSerializerTelefoonnummer();
        Telefoonnummer telefoonnummer = new Telefoonnummer();
        telefoonnummer.setTelefoonnummer("0612345678");

        JsonGenerator jsonGenerator = Mockito.mock(JsonGenerator.class);
        serializerTelefoonnummer.serialize(telefoonnummer, jsonGenerator, null);

        Mockito.verify(jsonGenerator).writeString(telefoonnummer.getTelefoonnummer());
    }
}