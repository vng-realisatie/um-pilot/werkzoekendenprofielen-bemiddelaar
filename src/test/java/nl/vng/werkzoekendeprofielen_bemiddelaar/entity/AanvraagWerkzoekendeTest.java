package nl.vng.werkzoekendeprofielen_bemiddelaar.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AanvraagWerkzoekendeTest {

    @Test
    void shouldAdd() {

        AanvraagWerkzoekende aanvraagWerkzoekende = new AanvraagWerkzoekende();
        assertTrue(aanvraagWerkzoekende.getWerkzoekenden().isEmpty());

        aanvraagWerkzoekende.addWerkzoekende(new MPWerkzoekendeMatch());

        assertEquals(aanvraagWerkzoekende.getWerkzoekenden().size(), 1);
    }

}