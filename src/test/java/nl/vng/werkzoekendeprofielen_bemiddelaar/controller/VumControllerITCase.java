package nl.vng.werkzoekendeprofielen_bemiddelaar.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.vng.werkzoekendeprofielen_bemiddelaar.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesCallbackRequest;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.ElkService;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.VumService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = VumController.class)
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
class VumControllerITCase {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private VumService service;
    
    @MockBean
    private ConfigProperties properties;

    @MockBean
    private ElkService elkService;
    private static final String PATH = "./src/test/resources/";
    private static WerkzoekendeProfielMatchesCallbackRequest callbackRequest;

    @BeforeEach
    void setUp() {
        try {
            callbackRequest = objectMapper.readValue(new File(PATH, "werkzoekende-profiel-matches-callback-request.json"),
                    WerkzoekendeProfielMatchesCallbackRequest.class);

        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void callbackWithHeaders() throws Exception {
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");
        
        mockMvc.perform(TestRequestFactory.postWithHeaders("/aanvraagwerkzoekende/callback")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(callbackRequest)))
                .andExpect(status().isOk());

    }

    @Test
    void callbackWithoutHeaders() throws Exception {
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");

        mockMvc.perform(post("/aanvraagwerkzoekende/callback")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(callbackRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Required request header")));
    }
}