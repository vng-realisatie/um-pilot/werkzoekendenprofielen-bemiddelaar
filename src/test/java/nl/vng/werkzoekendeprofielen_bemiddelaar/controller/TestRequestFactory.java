package nl.vng.werkzoekendeprofielen_bemiddelaar.controller;

import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class TestRequestFactory {

    public static MockHttpServletRequestBuilder postWithHeaders(String url) {
        return MockMvcRequestBuilders.post(url)
                .header("X-VUM-berichtVersie", "1.0.0")
                .header("X-VUM-vraagID", "123456")
                .header("X-VUM-toParty", "00000002821002197000")
                .header("X-VUM-fromParty", "00000001821002193000")
        		.header("X-Forwarded-For", "bb,serialNumber=01234567890123456789,OU=");
    }
}