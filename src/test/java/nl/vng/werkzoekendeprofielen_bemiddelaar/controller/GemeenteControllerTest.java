package nl.vng.werkzoekendeprofielen_bemiddelaar.controller;

import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.InlineResponse200;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesRequestGemeente;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.AanvraagWerkzoekende;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.ElkService;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.GemeenteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the GemeenteController
 */
@ExtendWith(MockitoExtension.class)
class GemeenteControllerTest {

    public static final String OIN = "OIN";
    public static final String VRAAG_ID = "VRAAG_ID";
    public static final String VUM_ID = "VUM_ID";
    @Mock
    private ElkService elkService;

    @Mock
    private GemeenteService gemeenteService;

    @Mock
    private SecurityContext securityContext;

    private JwtAuthenticationToken authentication;

    @InjectMocks
    private GemeenteController gemeenteController;

    @BeforeEach
    private void setUp() {
        authentication = new JwtAuthenticationToken(Jwt
                .withTokenValue("oin")
                .claim("oin", OIN)
                .header("", "")
                .build());
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void shouldCreateAanvraagVacatures() {

        WerkzoekendeProfielMatchesRequestGemeente requestGemeente = mock(WerkzoekendeProfielMatchesRequestGemeente. class);

        when(gemeenteService.makeRequestMatchesVum(requestGemeente, OIN)).thenReturn(new InlineResponse200());

        gemeenteController.createAanvraagWerkzoekende(requestGemeente, OIN);

        String requestSignature = "POST /aanvraagwerkzoekende/{oin}";
        String requestDescription = "Endpoint to post a request for werkzoekenden matching aanvraagWerkzoekende";
        String requestPath = "/aanvraagwerkzoekende/OIN";

        verify(elkService).handleRequest(requestGemeente, OIN, OIN, requestSignature, requestDescription, requestPath);
        verify(gemeenteService).makeRequestMatchesVum(requestGemeente, OIN);
    }

    @Test
    void shouldFindAll() {

        List<AanvraagWerkzoekende> aanvraagVacatures = Arrays.asList(new AanvraagWerkzoekende());
        when(gemeenteService.findAll(OIN)).thenReturn(aanvraagVacatures);

        List<AanvraagWerkzoekende> all = gemeenteController.allAanvraagWerkzoekendes(OIN);

        String requestSignature = "GET /aanvraagwerkzoekende/lijst/{oin}";
        String requestDescription = "Endpoint to retrieve all outstanding aanvragen for the given OIN";
        String requestPath = "/aanvraagwerkzoekende/lijst/OIN";

        verify(gemeenteService).findAll(OIN);
        verify(elkService).handleRequest(null, OIN, OIN, requestSignature, requestDescription, requestPath);

        assertEquals(aanvraagVacatures, all);
    }

    @Test()
    void shouldFailBuildingOinUserGetAll() {

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.allAanvraagWerkzoekendes("WRONG"), "WrongOinException is verwacht");
    }

    @Test()
    void shouldFailBuildingOinUserCreate() {

        WerkzoekendeProfielMatchesRequestGemeente requestGemeente = mock(WerkzoekendeProfielMatchesRequestGemeente. class);

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.createAanvraagWerkzoekende(requestGemeente, "WRONG"), "WrongOinException is verwacht");
    }

    @Test
    void shouldGetDetailVacature() {

        when(gemeenteService.makeRequestDetailWerkzoekende(VRAAG_ID, VUM_ID, OIN)).thenReturn(new Werkzoekende());

        Werkzoekende vacature = gemeenteController.getDetailWerkzoekende(OIN, VRAAG_ID, VUM_ID);

        String requestSignature = "GET /aanvraagwerkzoekende/detail/{oin}/{vraagId}/{vumId}";
        String requestDescription = "Endpoint to retrieve detailed werkzoekende for given vumID";
        String requestPath = "/aanvraagwerkzoekende/detail/OIN/VRAAG_ID/VUM_ID";

        verify(gemeenteService).makeRequestDetailWerkzoekende(VRAAG_ID, VUM_ID, OIN);
        verify(elkService).handleRequest(vacature, OIN, OIN, requestSignature, requestDescription, requestPath);

        assertNotNull(vacature);
    }

    @Test
    void shouldNotGetDetailVacature() {

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.getDetailWerkzoekende("WRONG", VRAAG_ID, VUM_ID), "WrongOinException is verwacht");

    }

    @Test
    void shouldGetAanvraagVacature() {

        when(gemeenteService.findByVraagIdAndOin(VRAAG_ID, OIN)).thenReturn(new AanvraagWerkzoekende());

        AanvraagWerkzoekende aanvraagWerkzoekende = gemeenteController.getAanvraagWerkzoekende(OIN, VRAAG_ID);

        String requestSignature = "GET /aanvraagwerkzoekende/{oin}/{vraagId}";
        String requestDescription = "Endpoint to retrieve AanvraagWerkzoekende for the given OIN with vraag ID";
        String requestPath = "/aanvraagwerkzoekende/OIN/VRAAG_ID";

        verify(gemeenteService).findByVraagIdAndOin(VRAAG_ID, OIN);
        verify(elkService).handleRequest(null, OIN, OIN, requestSignature, requestDescription, requestPath);

        assertNotNull(aanvraagWerkzoekende);
    }

    @Test
    void shouldNotGetAanvraagVacature() {

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.getAanvraagWerkzoekende("WRONG", VRAAG_ID), "WrongOinException is verwacht");

    }

}