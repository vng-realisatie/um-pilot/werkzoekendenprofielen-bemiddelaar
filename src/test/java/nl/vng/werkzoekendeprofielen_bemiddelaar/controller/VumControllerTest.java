package nl.vng.werkzoekendeprofielen_bemiddelaar.controller;

import nl.vng.werkzoekendeprofielen_bemiddelaar.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesCallbackRequest;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.ElkService;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.VumService;

import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.MismatchCertWithOinVumProviderException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the VumController
 */
@ExtendWith(MockitoExtension.class)
class VumControllerTest {

    @Mock
    private VumService vumService;

    @Mock
    private ElkService elkService;
    
    @Mock
    private ConfigProperties properties;

    @InjectMocks
    private VumController vumController;

    private final String xVumVraagId = "xVumVraagId";
    private final String xVUMBerichtVersie = "xVUMBerichtVersie";
    private final String xVUMToParty = "xVUMToParty";
    private final String xVUMViaParty = "xVUMViaParty";
    private final String xVUMFromParty = "xVUMFromParty";
    private final String xForwardedFor = "01234567890123456789";

    @Mock
    private WerkzoekendeProfielMatchesCallbackRequest matchesRequest;

    @Test
    void shouldHandleCallBack() {

        String requestSignature = "POST /aanvraagwerkzoekende/callback";
        String callback = "Callback methode om matching werkzoekenden binnen te krijgen voor Vraag ID";
        String requestPath = "/aanvraagwerkzoekende/callback";

        when(properties.getVumProviderOin()).thenReturn("00000001234567890123456789");
        ResponseEntity<Void> response = vumController.callback(xVUMBerichtVersie, xVUMToParty, xVUMFromParty, xVumVraagId, xForwardedFor, matchesRequest, xVUMViaParty);

        verify(elkService).handleRequest(matchesRequest, xVUMToParty, xVUMFromParty, requestSignature, callback, requestPath);
        verify(vumService).handleCallback(matchesRequest);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }
    
    @Test
    void callback_MismatchCertWithOinVumProvider() {
    	
        when(properties.getVumProviderOin()).thenReturn("01234567890123450000");
        
        Assertions.assertThrows(MismatchCertWithOinVumProviderException.class, () -> vumController.callback(xVUMBerichtVersie, xVUMToParty, xVUMFromParty, xVumVraagId, xForwardedFor, matchesRequest, xVUMViaParty));
    }
}