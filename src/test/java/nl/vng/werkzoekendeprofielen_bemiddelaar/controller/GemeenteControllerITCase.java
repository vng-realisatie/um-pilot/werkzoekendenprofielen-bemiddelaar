package nl.vng.werkzoekendeprofielen_bemiddelaar.controller;

import com.c4_soft.springaddons.security.oauth2.test.annotations.Claims;
import com.c4_soft.springaddons.security.oauth2.test.annotations.OpenIdClaims;
import com.c4_soft.springaddons.security.oauth2.test.annotations.StringClaim;
import com.c4_soft.springaddons.security.oauth2.test.annotations.WithMockJwtAuth;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.InlineResponse200;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesRequestGemeente;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.AanvraagWerkzoekende;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.ElkService;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.GemeenteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

import java.io.File;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = GemeenteController.class)
@ActiveProfiles("test")
class GemeenteControllerITCase {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private GemeenteService service;

    @MockBean
    private ElkService elkService;

    private static InlineResponse200 response;
    private static WerkzoekendeProfielMatchesRequestGemeente requestGemeente;
    private static AanvraagWerkzoekende aanvraagWerkzoekende;
    private static Werkzoekende werkzoekende;
    private static final String OIN = "123456789";
    private static final String INCORRECT_OIN = "INCORRECT_OIN";
    private static final String VUM_ID = "someid";
    private static final String VRAAG_ID = "somevraagid";
    private static final String PATH = "./src/test/resources/";

    @BeforeEach
    void setUp() {
        try {
            requestGemeente = objectMapper.readValue(new File(PATH, "werkzoekende-profiel-matches-request-gemeente.json"),
                    WerkzoekendeProfielMatchesRequestGemeente.class);
            response = objectMapper.readValue(new File(PATH, "inline-response200.json"),
                    InlineResponse200.class);
            aanvraagWerkzoekende = objectMapper.readValue(new File(PATH, "aanvraag-werkzoekende.json"),
                    AanvraagWerkzoekende.class);
            werkzoekende = objectMapper.readValue(new File(PATH, "werkzoekende.json"),
                    Werkzoekende.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @WithMockJwtAuth(authorities = { "ROLE_dummy" },claims = @OpenIdClaims(otherClaims = @Claims(stringClaims = @StringClaim(name = "oin", value = OIN))))
    void getAanvraagWerkzoekendeWithCorrectOIN() throws Exception {
        when(service.findByVraagIdAndOin(VRAAG_ID, OIN)).thenReturn(aanvraagWerkzoekende);

        mockMvc.perform(get("/aanvraagwerkzoekende/" + OIN + "/" + VRAAG_ID)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(aanvraagWerkzoekende)));
    }

    @Test
    @WithMockJwtAuth(authorities = {"ROLE_dummy"}, claims = @OpenIdClaims(otherClaims = @Claims(stringClaims = @StringClaim(name = "oin", value = INCORRECT_OIN))))
    void getAanvraagWerkzoekendeWithIncorrectOIN() throws Exception {

        mockMvc.perform(get("/aanvraagwerkzoekende/" + OIN + "/" + VRAAG_ID)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andExpect(content().string("{\"code\":\"403.01\",\"message\":\"Geen authorizatie\",\"details\":\"403 FORBIDDEN \\\"Invalide OIN\\\"\"}"));
    }

    @Test
    @WithMockJwtAuth(authorities = {"ROLE_dummy"}, claims = @OpenIdClaims(otherClaims = @Claims(stringClaims = @StringClaim(name = "oin", value = OIN))))
    void createAanvraagWerkzoekendeWithCorrectOIN() throws Exception {
        when(service.makeRequestMatchesVum(any(WerkzoekendeProfielMatchesRequestGemeente.class), eq(OIN))).thenReturn(response);

        mockMvc.perform(post("/aanvraagwerkzoekende/" + OIN).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(requestGemeente)))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(response)));
    }

    @Test
    @WithMockJwtAuth(authorities = {"ROLE_dummy"}, claims = @OpenIdClaims(otherClaims = @Claims(stringClaims = @StringClaim(name = "oin", value = INCORRECT_OIN))))
    void createAanvraagWerkzoekendeWithIncorrectOIN() throws Exception {
        mockMvc.perform(post("/aanvraagwerkzoekende/" + OIN).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(requestGemeente)))
                .andExpect(status().isForbidden())
                .andExpect(content().string("{\"code\":\"403.01\",\"message\":\"Geen authorizatie\",\"details\":\"403 FORBIDDEN \\\"Invalide OIN\\\"\"}"));
    }

    @Test
    @WithMockJwtAuth(authorities = {"ROLE_dummy"}, claims = @OpenIdClaims(otherClaims = @Claims(stringClaims = @StringClaim(name = "oin", value = OIN))))
    void getDetailWerkzoekendeWithCorrectOIN() throws Exception {
        when(service.makeRequestDetailWerkzoekende(VRAAG_ID, VUM_ID, OIN)).thenReturn(werkzoekende);

        mockMvc.perform(get("/aanvraagwerkzoekende/detail/" + OIN + "/" + VRAAG_ID + "/" + VUM_ID)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(werkzoekende)));
    }

    @Test
    @WithMockJwtAuth(authorities = {"ROLE_dummy"}, claims = @OpenIdClaims(otherClaims = @Claims(stringClaims = @StringClaim(name = "oin", value = INCORRECT_OIN))))
    void getDetailWerkzoekendeWithInCorrectOIN() throws Exception {
        mockMvc.perform(get("/aanvraagwerkzoekende/detail/" + OIN + "/" + VRAAG_ID + "/" + VUM_ID)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andExpect(content().string("{\"code\":\"403.01\",\"message\":\"Geen authorizatie\",\"details\":\"403 FORBIDDEN \\\"Invalide OIN\\\"\"}"));
    }

    @Test
    @WithMockJwtAuth(authorities = {"ROLE_dummy"}, claims = @OpenIdClaims(otherClaims = @Claims(stringClaims = @StringClaim(name = "oin", value = OIN))))
    void getAllAanvraagWerkzoekendeWithCorrectOIN() throws Exception {
        when(service.findAll(OIN)).thenReturn(List.of(aanvraagWerkzoekende));

        mockMvc.perform(get("/aanvraagwerkzoekende/lijst/" + OIN)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(List.of(aanvraagWerkzoekende))));
    }

    @Test
    @WithMockJwtAuth(authorities = {"ROLE_dummy"}, claims = @OpenIdClaims(otherClaims = @Claims(stringClaims = @StringClaim(name = "oin", value = INCORRECT_OIN))))
    void getAllAanvraagWerkzoekendenWithInCorrectOIN() throws Exception {

        mockMvc.perform(get("/aanvraagwerkzoekende/lijst/" + OIN)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andExpect(content().string("{\"code\":\"403.01\",\"message\":\"Geen authorizatie\",\"details\":\"403 FORBIDDEN \\\"Invalide OIN\\\"\"}"));
    }

}