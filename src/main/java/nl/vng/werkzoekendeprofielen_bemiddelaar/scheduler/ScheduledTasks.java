package nl.vng.werkzoekendeprofielen_bemiddelaar.scheduler;

import nl.vng.werkzoekendeprofielen_bemiddelaar.service.GemeenteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ScheduledTasks {

    private final GemeenteService service;

    public ScheduledTasks(final GemeenteService gemeenteService) {
        this.service = gemeenteService;
    }

    /**
     * Remove all expired aanvragen on a daily basis.
     */
    @Scheduled(fixedRate = 86400000) // one day in ms
    public void removeAllExpiredAanvraag() {
        log.info("Removing all expired aanvragen");
        service.removeAllExpiredAanvraag();
    }
}
