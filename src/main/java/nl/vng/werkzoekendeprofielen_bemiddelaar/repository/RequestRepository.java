package nl.vng.werkzoekendeprofielen_bemiddelaar.repository;

import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.ElkEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends ElasticsearchRepository<ElkEntity, String> {
}
