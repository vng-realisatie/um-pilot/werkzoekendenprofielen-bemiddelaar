package nl.vng.werkzoekendeprofielen_bemiddelaar.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Custom error class used in HTTP respones.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Error {

    @NotNull
    @Size(max = 10)
    private String code;

    @NotNull
    @Size(max = 500)
    private String message;

    @Size(max = 2000)
    private String details;
}
