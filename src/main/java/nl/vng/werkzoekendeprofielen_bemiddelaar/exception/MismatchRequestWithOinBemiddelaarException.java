package nl.vng.werkzoekendeprofielen_bemiddelaar.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class MismatchRequestWithOinBemiddelaarException extends ResponseStatusException {

    public MismatchRequestWithOinBemiddelaarException() {
        super(HttpStatus.FORBIDDEN, "Invalide OIN");
    }
}
