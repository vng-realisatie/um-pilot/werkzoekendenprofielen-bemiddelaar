package nl.vng.werkzoekendeprofielen_bemiddelaar.dto;

import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.Werkzoekende;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * InlineResponse2001
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InlineResponse2001 {
    @Size(max = 200)
    private String bronID;

    private Werkzoekende werkzoekende;

}
