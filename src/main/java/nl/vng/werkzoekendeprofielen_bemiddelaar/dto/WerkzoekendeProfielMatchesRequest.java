package nl.vng.werkzoekendeprofielen_bemiddelaar.dto;

import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.MPWerkzoekende;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WerkzoekendeProfielMatchesRequest {
    @NotNull
    private String callbackURL;

    @NotNull
    @Valid
    private MPWerkzoekende vraagObject;
}
