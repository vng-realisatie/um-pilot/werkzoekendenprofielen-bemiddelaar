package nl.vng.werkzoekendeprofielen_bemiddelaar.dto;

import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.MPWerkzoekende;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WerkzoekendeProfielMatchesRequestGemeente {
    @NotBlank
    private String aanvraagKenmerk;

    @NotNull
    @Valid
    private MPWerkzoekende vraagObject;

    @Override
    public String toString() {
        return "WerkzoekendeProfielMatchesRequestGemeente{" +
                "aanvraagKenmerk='" + aanvraagKenmerk + '\'' +
                ", vraagObject=" + vraagObject +
                '}';
    }
}
