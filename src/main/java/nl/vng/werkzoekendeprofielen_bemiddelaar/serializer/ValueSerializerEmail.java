package nl.vng.werkzoekendeprofielen_bemiddelaar.serializer;

import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.Emailadres;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class ValueSerializerEmail extends JsonSerializer<Emailadres> {
    @Override
    public void serialize(final Emailadres emailadres, final JsonGenerator jsonGenerator, final SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(emailadres.getEmailadres());
    }
}
