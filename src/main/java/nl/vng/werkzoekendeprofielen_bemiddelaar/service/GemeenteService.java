package nl.vng.werkzoekendeprofielen_bemiddelaar.service;

import nl.vng.werkzoekendeprofielen_bemiddelaar.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.InlineResponse200;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.InlineResponse2001;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesRequest;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesRequestGemeente;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.AanvraagWerkzoekende;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.MPWerkzoekendeMatch;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.NoMoreAccessRequestsLeftException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.UnprocessableException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.VraagIdAlreadyInDatabaseException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.VraagIdNotFoundException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.mapper.SimpleMapper;
import nl.vng.werkzoekendeprofielen_bemiddelaar.repository.AanvraagWerkzoekendeRepository;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class GemeenteService {

    public static final String X_VUM_BERICHT_VERSIE = "X-VUM-berichtVersie";
    public static final String X_VUM_FROM_PARTY = "X-VUM-fromParty";
    public static final String X_VUM_VIA_PARTY = "X-VUM-viaParty";
    public static final String X_VUM_TO_PARTY = "X-VUM-toParty";
    public static final String X_VUM_SUWIPARTY = "X-VUM-SUWIparty";

    private final AanvraagWerkzoekendeRepository repository;
    private final RestTemplate restTemplate;
    private final SimpleMapper mapper;
    private final ConfigProperties properties;

    public GemeenteService(final AanvraagWerkzoekendeRepository aanvraagWerkzoekendeRepository, final RestTemplate template, final SimpleMapper simpleMapper, final ConfigProperties configProperties) {
        this.repository = aanvraagWerkzoekendeRepository;
        this.restTemplate = template;
        this.mapper = simpleMapper;
        this.properties = configProperties;
    }


    public List<AanvraagWerkzoekende> findAll(String oin) {
        return repository.findByOin(oin);
    }


    public AanvraagWerkzoekende findByVraagIdAndOin(String vraagId, String oin) {
        AanvraagWerkzoekende aanvraagInDb = repository.findByVraagIdAndOin(vraagId, oin);
        if (aanvraagInDb == null) {
            throw new VraagIdNotFoundException();
        } else {
            return aanvraagInDb;
        }
    }

    /**
     * Make a rest call to VUM with the aanvraagWerkzoekende. Subsequently save the response vraag id in the database.
     *
     * @param aanvraagWerkzoekende AanvraagWerkzoekende to match on
     * @param oinBemiddelaar - identifier of werkzoekendeProfielen-bemiddelaar for example the requesting municipality
     * @return the response received from VUM
     */
    public InlineResponse200 makeRequestMatchesVum(final WerkzoekendeProfielMatchesRequestGemeente aanvraagWerkzoekende, final String oinBemiddelaar) {
        WerkzoekendeProfielMatchesRequest model = mapper.requestGemeenteToWerkzoekendeProfielMatchesRequest(aanvraagWerkzoekende);

        // Add callbackURL to request.
        model.setCallbackURL(properties.getCallbackUrl());
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(X_VUM_BERICHT_VERSIE, "1.0.0");
        headers.set(X_VUM_FROM_PARTY, oinBemiddelaar);
        if (!"".equals(properties.getBemiddelaarProviderOin())) {
        	headers.set(X_VUM_VIA_PARTY, properties.getBemiddelaarProviderOin());
        }
        headers.set(X_VUM_TO_PARTY, properties.getVumProviderOin());
        headers.set(X_VUM_SUWIPARTY, "true");

        HttpEntity<WerkzoekendeProfielMatchesRequest> matchesRequestEntity = new HttpEntity<>(model, headers);

        ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());
        
        String zoekparameters = null;
        try {
        	zoekparameters = mapper.writeValueAsString(aanvraagWerkzoekende);
        } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
        	log.error("Mapper error aanvraagWerkzoekende");
        	throw new UnprocessableException("Fout bij verwerken van de vraag", e);
        }
        
        InlineResponse200 response;
        try {
            response = restTemplate.postForObject(properties.getVumUrlMatches(), matchesRequestEntity, InlineResponse200.class);
            createAanvraagWerkzoekende(Objects.requireNonNull(response).getVraagID(), oinBemiddelaar, aanvraagWerkzoekende.getAanvraagKenmerk(), zoekparameters);
        } catch (VraagIdAlreadyInDatabaseException e) {
            log.error("Vraag id already in database");
            throw new UnprocessableException("Vraag id bestaat al in database");
        } catch (RestClientException e) {
            log.error("Rest client error in response from VUM after request for matches." + e.getMessage());
            throw new UnprocessableException("Rest client error");
        } catch (NullPointerException e) {
            log.error("NullPointer response from VUM after request for matches." + e.getMessage());
            throw new UnprocessableException("Rest client error");
        }
        return response;
    }

    /**
     * Save AanvraagWerkzoekende with given vraagID and OIN.
     *
     * @param vraagId - vraag id to save in the DB
     * @param aanvraagKenmerk
     * @param zoekparameters - de uitgezette vraag
     * @param oinBemiddelaar - identifier of werkzoekendeProfielen-bemiddelaar for example the requesting municipality
     * @throws VraagIdAlreadyInDatabaseException - if VraagID is already in the DB.
     */
    private void createAanvraagWerkzoekende(String vraagId, String oinBemiddelaar, String aanvraagKenmerk, String zoekparameters) throws VraagIdAlreadyInDatabaseException {
        if (repository.existsById(vraagId)) {
            throw new VraagIdAlreadyInDatabaseException();
        }
        repository.save(new AanvraagWerkzoekende(vraagId, oinBemiddelaar, aanvraagKenmerk, zoekparameters, properties.getMaxTimesAccessible()));
    }

    /**
     * Make a rest call to VUM for a detail werkzoekende.
     *
     * @param vraagId - identifier of the initial request
     * @param vumId   - identifier of the werkzoekende
     * @param oinBemiddelaar - identifier of werkzoekendeProfielen-bemiddelaar for example the requesting municipality
     * @return the response received from VUM
     */
    public Werkzoekende makeRequestDetailWerkzoekende(String vraagId, String vumId, String oinBemiddelaar) {
        AanvraagWerkzoekende aanvraagInDb = findByVraagIdAndOin(vraagId, oinBemiddelaar);

        ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.registerModule(new Jdk8Module());

        try {
            checkTimesAccessLeft(aanvraagInDb);
            // response = restTemplate.getForObject(properties.getVumUrlVumId() + vumId, InlineResponse2001.class);

            java.net.http.HttpRequest.Builder builder = HttpRequest.newBuilder()
                    .uri(new URI(properties.getVumUrlVumId() + vumId))
                    .header("Accept", "application/json")
                    .header(X_VUM_BERICHT_VERSIE, "1.0.0")
                    .header(X_VUM_FROM_PARTY, oinBemiddelaar)
                    .header(X_VUM_TO_PARTY, properties.getVumProviderOin())
                    .header(X_VUM_SUWIPARTY, "true")
                    .GET();
            
            if (!"".equals(properties.getBemiddelaarProviderOin())) {
            	builder.header(X_VUM_VIA_PARTY, properties.getBemiddelaarProviderOin());
            }
            
            HttpRequest request = builder.build();

            HttpResponse<String> clientResponse = HttpClient.newBuilder()
                    .build()
                    .send(request, HttpResponse.BodyHandlers.ofString());

            decreaseTimesAccessLeft(aanvraagInDb, vumId);
            markFetched(aanvraagInDb, vumId);

            InlineResponse2001 test = objectMapper.readValue(clientResponse.body(), InlineResponse2001.class);

            // return Objects.requireNonNull(test).getWerkzoekende();
            return test.getWerkzoekende();
        } catch (RestClientException | NullPointerException | IOException | InterruptedException |
                 URISyntaxException e) {
            log.error("Error in response from VUM after request for detailed werkzoekende." + e);
            throw new UnprocessableException("Rest client error");
        }
    }

    // Utility method to check if AanvraagWerkzoekende still has request left
    private void checkTimesAccessLeft(AanvraagWerkzoekende vraagIdInDb) {
        if (vraagIdInDb.getTimesAccessLeft() <= 0) {
            throw new NoMoreAccessRequestsLeftException();
        }
    }

    // Utility method to decrease requests left of AanvraagWerkzoekende
    private void decreaseTimesAccessLeft(AanvraagWerkzoekende vraagIdInDb, String vumId) {
        MPWerkzoekendeMatch werkzoekende = vraagIdInDb.getWerkzoekenden()
                .stream()
                .filter(mpWerkzoekendeMatch -> mpWerkzoekendeMatch.getVumID().equals(vumId))
                .findAny()
                .orElse(null);

        if (werkzoekende != null && werkzoekende.isFetchedDetail()) {
            return;
        }
        checkTimesAccessLeft(vraagIdInDb);

        vraagIdInDb.setTimesAccessLeft(vraagIdInDb.getTimesAccessLeft() - 1);
        repository.save(vraagIdInDb);
    }

    // Utility method to mark a werkzoekende as fetched
    private void markFetched(AanvraagWerkzoekende vraagIdInDb, String vumId) {
        vraagIdInDb.getWerkzoekenden().forEach(werkzoekende -> {
            if (werkzoekende.getVumID().equals(vumId)) {
                werkzoekende.setFetchedDetail(true);
            }
        });
        repository.save(vraagIdInDb);
    }

    /**
     * Remove all expired aanvragen from the specified days in the application properties.
     */
    public void removeAllExpiredAanvraag() {
        LocalDate expirydate = LocalDate.now().minusDays(properties.getDaysToExpiry());
        List<AanvraagWerkzoekende> expiredAanvragen = repository.findAllByCreatieDatumBefore(expirydate);
        repository.deleteAll(expiredAanvragen);
    }

}
