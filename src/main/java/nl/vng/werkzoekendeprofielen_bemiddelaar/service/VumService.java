package nl.vng.werkzoekendeprofielen_bemiddelaar.service;

import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesCallbackRequest;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.AanvraagWerkzoekende;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.MPWerkzoekendeMatch;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.VraagIdNotFoundException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.repository.AanvraagWerkzoekendeRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VumService {

    private final AanvraagWerkzoekendeRepository repository;

    public VumService(final AanvraagWerkzoekendeRepository werkzoekendeRepository) {
        this.repository = werkzoekendeRepository;
    }

    /**
     * Handle callback. Either the AanvraagWerkzoekende is already in the DB or it still has to be added.
     *
     * @param matchesRequest - callback request received from VUM
     */
    public void handleCallback(final WerkzoekendeProfielMatchesCallbackRequest matchesRequest) {
        Optional<AanvraagWerkzoekende> aanvraagInDbOpt = repository.findById(matchesRequest.getVraagID());
        if (aanvraagInDbOpt.isEmpty()) {
            throw new VraagIdNotFoundException();
        } else {
            AanvraagWerkzoekende aanvraagInDb = aanvraagInDbOpt.get();
            for (MPWerkzoekendeMatch werkzoekendeMatch : matchesRequest.getMatches().getMpWerkzoekendeMatches()) {
                aanvraagInDb.addWerkzoekende(werkzoekendeMatch);
            }
            repository.save(aanvraagInDb);
        }
    }
}
