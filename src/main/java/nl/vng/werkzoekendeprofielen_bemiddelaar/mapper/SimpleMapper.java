package nl.vng.werkzoekendeprofielen_bemiddelaar.mapper;


import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesRequest;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesRequestGemeente;
import org.mapstruct.Mapper;

/**
 * Interface from which Mapstruct generates a mapper between classes.
 */
@Mapper(
        componentModel = "spring")
public interface SimpleMapper {

    WerkzoekendeProfielMatchesRequest requestGemeenteToWerkzoekendeProfielMatchesRequest(WerkzoekendeProfielMatchesRequestGemeente werkzoekendeProfielMatchesRequestGemeente);
}
