package nl.vng.werkzoekendeprofielen_bemiddelaar.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class OpleidingsnaamOngecodeerdImpl {
    @Size(max = 120)
    @Schema(description = "De omschrijving van de OPLEIDING.")
    private String naamOpleidingOngecodeerd;

    @Size(max = 2000)
    @Schema(description = "De eigen omschrijving van de opleiding.")
    private String omschrijvingOpleiding;

    @Override
    public String toString() {
        return "OpleidingsnaamOngecodeerdImpl{" +
                "naamOpleidingOngecodeerd='" + naamOpleidingOngecodeerd + '\'' +
                ", omschrijvingOpleiding='" + omschrijvingOpleiding + '\'' +
                '}';
    }
}
