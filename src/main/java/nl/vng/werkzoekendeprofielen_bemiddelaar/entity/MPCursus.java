package nl.vng.werkzoekendeprofielen_bemiddelaar.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.time.LocalDate;

/**
 * MPCursus
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@Embeddable
@JsonInclude(Include.NON_NULL)
@Schema(description = "De cursus die de werkzoekende volgt, heeft gevolgd of heeft ingepland om te gaan volgen.")
public class MPCursus {

    @Size(max = 120)
    @Schema(description = "De naam van de cursus zoals deze bekend staat.")
    private String naamCursus;
    
    @JsonFormat(pattern="yyyy-MM-dd")
    @Schema(description = "De datum van de dag die op het certificaat staat.")
    private LocalDate datumCertificaat;

    @Override
    public String toString() {
        return "MPCursus{" +
                "naamCursus='" + naamCursus + '\'' +
                ", datumCertificaat=" + datumCertificaat +
                '}';
    }
}
