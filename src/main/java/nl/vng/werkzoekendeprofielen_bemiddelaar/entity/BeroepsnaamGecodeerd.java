package nl.vng.werkzoekendeprofielen_bemiddelaar.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.validation.Valid;


/**
 * BeroepsnaamGecodeerd
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class BeroepsnaamGecodeerd extends Beroepsnaam {

    @Valid
    private BeroepsnaamGecodeerdImpl beroepsnaamGecodeerd;

    @Override
    public String toString() {
        return "BeroepsnaamGecodeerd{" +
                "beroepsnaamGecodeerd=" + beroepsnaamGecodeerd +
                '}';
    }
}
