package nl.vng.werkzoekendeprofielen_bemiddelaar.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Transient;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AanvraagWerkzoekende {

    @Size(max = 100)
    @Id
    private String vraagId;

    @OneToMany( //TODO: Many to many requires less data, but has orphan removal problem.
            mappedBy = "aanvraagWerkzoekenden",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Transient
    private Set<MPWerkzoekendeMatch> werkzoekenden = new HashSet<>();

    @JsonIgnore
    private String oin;
    
    @Column(columnDefinition = "TEXT")
    private String zoekparameters;

    @NotBlank
    private String aanvraagKenmerk;

    private int timesAccessLeft;

    private LocalDate creatieDatum = LocalDate.now();

    public AanvraagWerkzoekende(final String id, final String theOin, final String kenmerk, final String zoekparameters, final Integer timesLeft) {
        this.vraagId = id;
        this.oin = theOin;
        this.aanvraagKenmerk = kenmerk;
        this.timesAccessLeft = timesLeft;
    	this.zoekparameters=zoekparameters;
    }
    
    public void addWerkzoekende(final MPWerkzoekendeMatch werkzoekendeMatch) {
        werkzoekenden.add(werkzoekendeMatch);
        werkzoekendeMatch.setAanvraagWerkzoekenden(this);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        AanvraagWerkzoekende that = (AanvraagWerkzoekende) o;
        return Objects.equals(vraagId, that.vraagId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vraagId);
    }

    @Override
    public String toString() {
        return "AanvraagWerkzoekende{" +
                "vraagId='" + vraagId + '\'' +
                ", oin='" + oin + '\'' +
                ", aanvraagKenmerk='" + aanvraagKenmerk + '\'' +
                ", timesAccessLeft=" + timesAccessLeft +
                ", creatieDatum=" + creatieDatum +
                '}';
    }
}
