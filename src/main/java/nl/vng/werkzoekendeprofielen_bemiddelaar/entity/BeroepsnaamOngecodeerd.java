package nl.vng.werkzoekendeprofielen_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.validation.Valid;

/**
 * BeroepsnaamOngecodeerd
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class BeroepsnaamOngecodeerd extends Beroepsnaam {

    @Valid
   private BeroepsnaamOngecodeerdImpl beroepsnaamOngecodeerd;

    @Override
    public String toString() {
        return "BeroepsnaamOngecodeerd{" +
                "beroepsnaamOngecodeerd=" + beroepsnaamOngecodeerd +
                '}';
    }
}
