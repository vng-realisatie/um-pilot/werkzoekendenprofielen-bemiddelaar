package nl.vng.werkzoekendeprofielen_bemiddelaar.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

/**
 * MPWerkervaring
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@Embeddable
@Schema(description = "De werkzaamheden die een natuurlijk persoon in een bepaalde periode als beroep heeft uitgevoerd.")
public class MPWerkervaring {

    @JsonFormat(pattern="yyyy-MM-dd")
    @Schema(description = "De datum van de eerste dag van de werkzaamheden.")
    private LocalDate datumAanvangWerkzaamheden;

    @JsonFormat(pattern="yyyy-MM-dd")
    @Schema(description = "De datum van de laatste dag van de werkzaamheden.")
    private LocalDate datumEindeWerkzaamheden;

    @Size(max = 70)
    @Schema(description = "De naam waaronder de organisatie bekend is.")
    private String naamOrganisatie;

    @Valid
    @OneToOne(cascade = {CascadeType.ALL})
    private Beroepsnaam beroep;

    @Override
    public String toString() {
        return "MPWerkervaring{" +
                "datumAanvangWerkzaamheden=" + datumAanvangWerkzaamheden +
                ", datumEindeWerkzaamheden=" + datumEindeWerkzaamheden +
                ", naamOrganisatie='" + naamOrganisatie + '\'' +
                ", beroep=" + beroep +
                '}';
    }
}
