package nl.vng.werkzoekendeprofielen_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.validation.Valid;

/**
 * OpleidingsnaamOngecodeerd
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class OpleidingsnaamOngecodeerd extends Opleidingsnaam {

    @Valid
    private OpleidingsnaamOngecodeerdImpl opleidingsnaamOngecodeerd;

    @Override
    public String toString() {
        return "OpleidingsnaamOngecodeerd{" +
                "opleidingsnaamOngecodeerd=" + opleidingsnaamOngecodeerd +
                '}';
    }
}
