package nl.vng.werkzoekendeprofielen_bemiddelaar.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

/**
 * Cursus
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "De cursus die de werkzoekende volgt, heeft gevolgd of heeft ingepland om te gaan volgen.")
public class Cursus extends MPCursus {

    @JsonFormat(pattern="yyyy-MM-dd")
    @Schema(description = "De datum van de eerste dag waarop de cursus wordt of is gevolgd.")
    private LocalDate datumAanvangVolgenCursus;

    @JsonFormat(pattern="yyyy-MM-dd")
    @Schema(description = "De datum van de laatste dag dat de cursus is gevolgd.")
    private LocalDate datumEindeVolgenCursus;

    @Size(max = 500)
    @Schema(description = "De naam van het instituut waar de opleiding of cursus is gevolgd.")
    private String naamOpleidingsinstituut;

    @Override
    public String toString() {
        return "Cursus{" +
                "datumAanvangVolgenCursus=" + datumAanvangVolgenCursus +
                ", datumEindeVolgenCursus=" + datumEindeVolgenCursus +
                ", naamOpleidingsinstituut='" + naamOpleidingsinstituut + '\'' +
                '}';
    }
}
