package nl.vng.werkzoekendeprofielen_bemiddelaar.controller;

import nl.vng.werkzoekendeprofielen_bemiddelaar.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesCallbackRequest;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.MismatchCertWithOinVumProviderException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.ElkService;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.VumService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

@RestController
@Validated
@Slf4j
@CrossOrigin()
public class VumController {
	
	public static final String X_VUM_BERICHT_VERSIE = "X-VUM-berichtVersie";
    public static final String X_VUM_FROM_PARTY = "X-VUM-fromParty";
    public static final String X_VUM_VIA_PARTY = "X-VUM-viaParty";
    public static final String X_VUM_TO_PARTY = "X-VUM-toParty";
    public static final String X_VUM_SUWIPARTY = "X-VUM-SUWIparty";
    public static final String X_FORWARDED_FOR = "X-Forwarded-For";

    private final VumService service;

    private final ElkService elkService;
    
    private final ConfigProperties config;

    public VumController(final VumService vumService, final ElkService elk, final ConfigProperties configProperties) {
        this.service = vumService;
        this.elkService = elk;
        this.config = configProperties;
    }

    @ApiOperation(value = "callback methode", nickname = "callback", notes = "Callback methode om matching werkzoekenden binnen te krijgen voor Vraag ID.", tags = {})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Fout bij uitvoeren van zoekvraag"),
            @ApiResponse(responseCode = "401", description = "not authorized")})
    @PostMapping(
            value = "/aanvraagwerkzoekende/callback",
            consumes = {"application/json"})
    public ResponseEntity<Void> callback(final @Parameter(required = true) @RequestHeader(value = X_VUM_BERICHT_VERSIE, required = true) String xVUMBerichtVersie,
                                         final @Pattern(regexp="^\\d{20}$")  @Parameter(required = true) @RequestHeader(value = X_VUM_TO_PARTY, required = true) String xVUMToParty,
                                         final @Pattern(regexp="^\\d{20}$") @Parameter(required = true) @RequestHeader(value = X_VUM_FROM_PARTY, required = true) String xVUMFromParty,
                                         final @Parameter(required = true) @RequestHeader(value = "X-VUM-vraagID", required = true) String xVUMVraagID,
                                         final @Parameter(required = true) @RequestHeader(value = X_FORWARDED_FOR, required = true) String xForwardedFor,
                                         final @Parameter(required = true) @Valid @RequestBody WerkzoekendeProfielMatchesCallbackRequest matchesRequest,
                                         final @Pattern(regexp="^\\d{20}$")  @Parameter() @RequestHeader(value = X_VUM_VIA_PARTY, required = false) String xVUMViaParty) {

        log.info("Callback ontvangen voor toParty OIN: " + xVUMToParty + " voor vraagID: " + xVUMVraagID);
        log.info("Receiving vraagobject {}", matchesRequest);
        log.info("Receiving Headers xVUMBerichtVersie={}, xVUMToParty={}, xVUMViaParty={}, xVUMFromParty={}, xVUMVraagID={}", xVUMBerichtVersie,xVUMToParty,xVUMViaParty, xVUMFromParty, xVUMVraagID);
        
        validateXForwardedForHeader(xForwardedFor);

        service.handleCallback(matchesRequest);
        log.info("Succesvolle afhandeling callback voor toParty OIN: " + xVUMToParty + " voor vraagID: " + xVUMVraagID);
        
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set(X_VUM_BERICHT_VERSIE, "1.0.0");
        responseHeaders.set(X_VUM_FROM_PARTY, xVUMToParty);
        if (!"".equals(xVUMViaParty)) {            	
        	responseHeaders.set(X_VUM_VIA_PARTY, xVUMViaParty);
        }
        responseHeaders.set(X_VUM_TO_PARTY, xVUMFromParty);
        
        elkService.handleRequest(
                matchesRequest,
                xVUMToParty,
                xVUMFromParty,
                "POST /aanvraagwerkzoekende/callback",
                "Callback methode om matching werkzoekenden binnen te krijgen voor Vraag ID",
                "/aanvraagwerkzoekende/callback"
        );
        return new ResponseEntity<>(responseHeaders, HttpStatus.OK);
    }
    
    /**
     * Indien xForwardedFor geen VUM provider OIN gooi exceptie.
     * Gebruik de VUM provider OIN zonder voorloop nullen
     * 
     * @param xForwardedFor
     */
    private void validateXForwardedForHeader(String xForwardedFor) {
    	String vumProviderOinTrimmed = config.getVumProviderOin().replaceFirst("^0+(?!$)", "");
    	if (!xForwardedFor.contains(vumProviderOinTrimmed)) {
        	log.info(X_FORWARDED_FOR + "='"+ xForwardedFor + "'");
            throw new MismatchCertWithOinVumProviderException(); 
        }		
	}

}

