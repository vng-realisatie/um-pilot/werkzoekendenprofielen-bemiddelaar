package nl.vng.werkzoekendeprofielen_bemiddelaar.controller;

import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.InlineResponse200;
import nl.vng.werkzoekendeprofielen_bemiddelaar.dto.WerkzoekendeProfielMatchesRequestGemeente;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.AanvraagWerkzoekende;
import nl.vng.werkzoekendeprofielen_bemiddelaar.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bemiddelaar.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.ElkService;
import nl.vng.werkzoekendeprofielen_bemiddelaar.service.GemeenteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@Slf4j
@CrossOrigin()
public class GemeenteController {

    private final GemeenteService service;

    private final ElkService elkService;

    @Autowired
    public GemeenteController(final GemeenteService gemeenteService, final ElkService elk) {
        this.service = gemeenteService;
        this.elkService = elk;
    }

    /**
     * GET /aanvraagwerkzoekende/lijst/{oin} : Endpoint to retrieve all outstanding aanvragen for the given OIN.
     *
     * @param oin identifier of municipality
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Internal Server Error (status code 500)
     */
    @GetMapping("/aanvraagwerkzoekende/lijst/{oin}")
    public List<AanvraagWerkzoekende> allAanvraagWerkzoekendes(final @PathVariable String oin) {
        log.info("Verzoek voor alle uitstaande werkzoekenden aanvragen van OIN: " + oin);
        if (getOinUser().equals(oin)) {
            List<AanvraagWerkzoekende> result = service.findAll(oin);
            log.info("Succesvolle verzoek voor alle uitstaande werkzoekenden aanvragen van OIN: " + oin);
            elkService.handleRequest(
                null,
                oin,
                getOinUser(),
                "GET /aanvraagwerkzoekende/lijst/{oin}",
                "Endpoint to retrieve all outstanding aanvragen for the given OIN",
                "/aanvraagwerkzoekende/lijst/" + oin
            );
            return result;
        } else {
            log.info("Mislukte verzoek voor alle uitstaande werkzoekenden door OIN: " + getOinUser() + "Er is geprobeerd te uploaden voor OIN:" + oin);
            elkService.handleRequest(
                    new MismatchRequestWithOinBemiddelaarException(),
                    oin,
                    getOinUser(),
                    "GET /aanvraagwerkzoekende/lijst/{oin}",
                    "Endpoint to retrieve all outstanding aanvragen for the given OIN",
                    "/aanvraagwerkzoekende/lijst/" + oin
            );
            throw new MismatchRequestWithOinBemiddelaarException();
        }
    }

    /**
     * GET /aanvraagwerkzoekende/{oin}/{vraagId} : Endpoint to retrieve AanvraagWerkzoekende for the given OIN with vraag ID.
     *
     * @param oin     identifier of municipality
     * @param vraagId identifier of AanvraagWerkzoekende.
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Unprocessable request (status code 422)
     * or Internal Server Error (status code 500)
     */
    @GetMapping("/aanvraagwerkzoekende/{oin}/{vraagId}")
    public AanvraagWerkzoekende getAanvraagWerkzoekende(final @PathVariable String oin, final @PathVariable String vraagId) {
        log.info("Verzoek voor werkzoekende met vraagID: " + vraagId + " van OIN: " + oin);
        if (getOinUser().equals(oin)) {
            AanvraagWerkzoekende result = service.findByVraagIdAndOin(vraagId, oin);
            log.info("Succesvolle verzoek voor werkzoekende met vraagID: " + vraagId + " van OIN: " + oin);
            elkService.handleRequest(
                    null,
                    oin,
                    getOinUser(),
                    "GET /aanvraagwerkzoekende/{oin}/{vraagId}",
                    "Endpoint to retrieve AanvraagWerkzoekende for the given OIN with vraag ID",
                    "/aanvraagwerkzoekende/" + oin + "/" + vraagId
            );
            return result;
        } else {
            log.info("Mislukte verzoek voor werkzoekende met vraagID: " + vraagId + " van OIN: " + oin);
            elkService.handleRequest(
                    new MismatchRequestWithOinBemiddelaarException(),
                    oin,
                    getOinUser(),
                    "GET /aanvraagwerkzoekende/{oin}/{vraagId}",
                    "Endpoint to retrieve AanvraagWerkzoekende for the given OIN with vraag ID",
                    "/aanvraagwerkzoekende/" + oin + "/" + vraagId
            );
            throw new MismatchRequestWithOinBemiddelaarException();
        }
    }


    /**
     * GET /aanvraagwerkzoekende/{oin}/{vumId} : Endpoint to retrieve detailed werkzoekende for given vumID.
     *
     * @param oin     identifier of municipality
     * @param vraagId identifier of the AanvraagWerkzoekende
     * @param vumId   identifier of werkzoekende
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Unprocessable request (status code 422)
     * or Internal Server Error (status code 500)
     */
    @GetMapping("/aanvraagwerkzoekende/detail/{oin}/{vraagId}/{vumId}")
    public Werkzoekende getDetailWerkzoekende(final @PathVariable String oin, final @PathVariable String vraagId, final @PathVariable String vumId) {
        log.info("Verzoek voor detail werkzoekende met vraagID: " + vraagId + " vumID: " + vumId + " van OIN: " + oin);
        if (getOinUser().equals(oin)) {
            Werkzoekende result = service.makeRequestDetailWerkzoekende(vraagId, vumId, oin);
            log.info("Succesvolle verzoek voor detail werkzoekende met vraagID: " + vraagId + " vumID: " + vumId + " van OIN: " + oin);
            elkService.handleRequest(
                    result,
                    oin,
                    getOinUser(),
                    "GET /aanvraagwerkzoekende/detail/{oin}/{vraagId}/{vumId}",
                    "Endpoint to retrieve detailed werkzoekende for given vumID",
                    "/aanvraagwerkzoekende/detail/" + oin + "/" + vraagId + "/" + vumId
            );
            return result;
        } else {
            log.info("Mislukte verzoek voor detail werkzoekende met vraagID: " + vraagId + " vumID: " + vumId + " van OIN: " + oin);
            elkService.handleRequest(
                    new MismatchRequestWithOinBemiddelaarException(),
                    oin,
                    getOinUser(),
                    "GET /aanvraagwerkzoekende/detail/{oin}/{vraagId}/{vumId}",
                    "Endpoint to retrieve detailed werkzoekende for given vumID",
                    "/aanvraagwerkzoekende/detail/" + oin + "/" + vraagId + "/" + vumId
            );
            throw new MismatchRequestWithOinBemiddelaarException();
        }
    }


    /**
     * POST /aanvraagwerkzoekende/{oin} : Endpoint to post a request for werkzoekenden matching aanvraagWerkzoekende.
     *
     * @param aanvraagWerkzoekende AanvraagWerkzoekende to match against
     * @param oin                  identifier of municipality
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Unprocessable request (status code 422)
     * or Internal Server Error (status code 500)
     */
    @PostMapping("/aanvraagwerkzoekende/{oin}")
    public InlineResponse200 createAanvraagWerkzoekende(final @Valid @RequestBody WerkzoekendeProfielMatchesRequestGemeente aanvraagWerkzoekende,
                                                        final @PathVariable String oin) {
        log.info("Verzoek voor werkzoekende van OIN: " + oin);
        if (getOinUser().equals(oin)) {
            InlineResponse200 result = service.makeRequestMatchesVum(aanvraagWerkzoekende, oin);
            log.info("Succesvolle verzoek voor werkzoekende van OIN: " + oin + " Verzoek heeft gekregen vraagID: " + result.getVraagID());
            log.info("Sending vraagObject {}", aanvraagWerkzoekende);
            elkService.handleRequest(
                    aanvraagWerkzoekende,
                    oin,
                    getOinUser(),
                    "POST /aanvraagwerkzoekende/{oin}",
                    "Endpoint to post a request for werkzoekenden matching aanvraagWerkzoekende",
                    "/aanvraagwerkzoekende/" + oin
            );
            return result;
        } else {
            log.info("Mislukte verzoek voor werkzoekende van OIN: " + getOinUser() + "Er is geprobeerd een verzoek in te dienen voor OIN:" + oin);
            elkService.handleRequest(
                    new MismatchRequestWithOinBemiddelaarException(),
                    oin,
                    getOinUser(),
                    "POST /aanvraagwerkzoekende/{oin}",
                    "Endpoint to post a request for werkzoekenden matching aanvraagWerkzoekende",
                    "/aanvraagwerkzoekende/" + oin
            );
            throw new MismatchRequestWithOinBemiddelaarException();
        }
    }


    /**
     * Utility method to extract OIN from the jwt token given by the user.
     *
     * @return OIN of the user or empty string if no OIN given.
     */
    private String getOinUser() {
        Authentication authentication = (Authentication)
                SecurityContextHolder.getContext().getAuthentication();

        String oinUser = ((JwtAuthenticationToken) authentication).getToken().getClaim("oin");
        
        return oinUser == null ?  "" : oinUser;
    }
}
