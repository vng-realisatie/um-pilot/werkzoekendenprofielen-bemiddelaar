# Feature : US_WBM_004 Als Bemiddelaar wil ik de detailgegevens van een werkzoekendeprofielen aanvraag kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum         | opmerking                 |
|--------|---------------|---------------------------|
| 0.10   | februari 2023 | Initiele opzet            |

**Als** Bemiddelaar  
**Wil ik** de detailgegevens van een werkzoekendeprofielen aanvraag kunnen raadplegen
**Zodat ik** dit werkzoekendeprofiel kan bemiddelen

### Functioneel
Dit is een ondersteunende User Story die beschrijft waaraan de API dient te voldoen.

Voor het bemiddelen van een geschikt werkzoekendeprofiel is het nodig de details van dit werkzoekendeprofiel ter beschikking te hebben

### Technische Documentatie

Endpoint : "/aanvraagwerkzoekende/detail/{oin}/{vraagId}/{vumId}"

In de implementatie worden de volgende response codes genoemd
"200", description = "OK"
"400", description = "Bad request"
"403", description = "not authorized"
"422", description = "Fout bij uitvoeren van zoekvraag"
"429", description = "Too Many Requests, limiet is overschreden voor deze uitvraag"
"500", description = "Internal Server Error"
"503", description = "Service Unavailable"


#### Postconditie
Na een bewerking is altijd de log bijgewerkt

### Acceptatiecriteria

*Feature: Opvragen details werkzoekendeprofiel aanvraag*  
**Gegeven** de Client is geauthoriseerd  
**En** er bestaat een aanvraag werkzoekendeprofiel met een gegeven id
**Wanneer** de details van een aanvraag werkzoekendeprofiel voor een OIN met het gegeven id wordt opgevraagd  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** is werkzoekendeprofiel aanvraag inclusief relevante werkzoekendeprofielen opgenomen in de response
**En** het aantal beschikbare opvragingen verminderd met 1
**En** zijn de werkzoekendeprofielen gemarkeerd als bevraagd

*Feature: Opvragen niet bestaande werkzoekendeprofiel aanvraag*  
**Gegeven** de Client is geauthoriseerd  
**En** er bestaat een aanvraag werkzoekendeprofiel met een gegeven id
**Wanneer** een aanvraag werkzoekendeprofiel voor een OIN met het gegeven id wordt opgevraagd  
**Dan** retourneert de applicatie een http status 400 (Ongeldige aanroep)  

*Feature: Te veel bevragingen op werkzoekendeprofiel aanvraag*  
**Gegeven** de Client is geauthoriseerd  
**En** er bestaat een aanvraag werkzoekendeprofiel met een gegeven id
**En** het aantal toegestane bevragingen van deze aanvraag is bereikt
**Wanneer** de details van een aanvraag werkzoekendeprofiel voor een OIN met het gegeven id wordt opgevraagd
**Dan** retourneert de applicatie een http status 429 (Too Many Requests)

*Scenario: niet geautoriseerd*  
**Gegeven** de Client is niet geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 401 (not authorized)  

*Scenario: verwerking niet valide JSON*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een niet valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 400 (Bad request)  
