# Feature : US_WBM_002 Als Bemiddelaar wil ik bestaande aanvragen naar werkzoekendeprofielen kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum         | opmerking                 |
|--------|---------------|---------------------------|
| 0.10   | februari 2023 | Initiele opzet            |

**Als** Bemiddelaar  
**Wil ik** bestaande aanvragen naar werkzoekendeprofielen kunnen raadplegen
**Zodat ik** kan beoordelen welke aanvragen relevant zijn voor bemiddeling

### Functioneel
Dit is een ondersteunende User Story die beschrijft waaraan de API dient te voldoen.

Voor het beoordelen of er werkzoekendeprofielen bestaan op bepaalde criteria of het raadplegen van openstaande vragen, dient het mogelijk te zijn een overzicht per OIN te bieden welke aanvragen reeds lopen.


### Technische Documentatie

Endpoint : "/aanvraagwerkzoekende/lijst/{oin}"

In de implementatie worden de volgende response codes genoemd
"200", description = "OK"
"400", description = "Bad request"
"403", description = "not authorized"
"422", description = "Fout bij uitvoeren van zoekvraag"
"429", description = "Too Many Requests, limiet is overschreden voor deze uitvraag"
"500", description = "Internal Server Error"
"503", description = "Service Unavailable"


#### Postconditie
Na een bewerking is altijd de log bijgewerkt

### Acceptatiecriteria

*Feature: Opvragen vacature aanvragen*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** de lijst met aanvragen voor een OIN wordt opgevraagd  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** zijn de aanvragen opgenomen in de response

*Scenario: niet geautoriseerd*  
**Gegeven** de Client is niet geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 401 (not authorized)  

*Scenario: verwerking niet valide JSON*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een niet valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 400 (Bad request)  
