# Feature : US_WBM_001 Als Bemiddelaar wil ik een aanvraag naar werkzoekendeprofielen kunnen registreren

versie 0.10

_Versionering_

| versie | datum         | opmerking                 |
|--------|---------------|---------------------------|
| 0.10   | februari 2023 | Initiele opzet            |

**Als** Bemiddelaar  
**Wil ik** een aanvraag naar werkzoekendeprofielen kunnen registreren
**Zodat ik** bij een vacature matchende werkzoekendeprofielen kan vinden

### Functioneel
Dit is een ondersteunende User Story die beschrijft waaraan de API dient te voldoen.

Bij het aanvragen van een werkzoekendeprofielen op basis van een aantal criteria wordt een vraag ingeschoten. 
Deze vraag wordt geregistreerd in de bemiddelaar. 
Middels een callback worden de werkzoekendeprofielen die matchen op de gegeven criteria opgehaald uit een vum-bron.
De gevonden werkzoekendeprofielen worden in de bemiddelaar bij de vraag opgeslagen. 

### Technische Documentatie

Endpoint : "/aanvraagwerkzoekende/{oin}"
Bij het aanroepen van deze method wordt impliciet een call gedaan naar VUM via een callback. 
De callback zelf wordt verder niet beschreven. 
Voor het resultaat van de call back wordt verwezen naar de userstories in het werkzoekendeprofielen-bron project.

In de implementatie worden de volgende response codes genoemd
"200", description = "OK"
"400", description = "Bad request"
"403", description = "not authorized"
"422", description = "Fout bij uitvoeren van zoekvraag"
"429", description = "Too Many Requests, limiet is overschreden voor deze uitvraag"
"500", description = "Internal Server Error"
"503", description = "Service Unavailable"


#### Postconditie
Na een bewerking is altijd de log bijgewerkt

### Acceptatiecriteria

*Feature: Toevoegen aanvraag vacatures*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** is de aanvraag met de bijbehorende referenties naar werkzoekendeprofielen opgenomen in de bemiddelaar
**En** is het maximaal aantal mogelijke aanvragen gezet op x

*Scenario: niet geautoriseerd*  
**Gegeven** de Client is niet geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 401 (not authorized)  
**En** is de aanvraag niet opgenomen in de bron  

*Scenario: verwerking niet valide JSON*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een niet valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 400 (Bad request)  
**En** is de aanvraag niet opgenomen in de bron 
