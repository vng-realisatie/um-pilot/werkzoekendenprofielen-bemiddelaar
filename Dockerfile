FROM adoptopenjdk/openjdk11

ARG ARG_APP_VERSION=0.0.1-SNAPSHOT

ENV TZ=Europe/Amsterdam

EXPOSE 8080

COPY target/werkzoekendeprofielen_bemiddelaar-${ARG_APP_VERSION}.jar werkzoekendenprofielen-bemiddelaar.jar

ENTRYPOINT ["java","-jar","/werkzoekendenprofielen-bemiddelaar.jar"]
